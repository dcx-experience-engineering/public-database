# public-database

## example
```
{
    "index": 1,
    "structure": [
        { "name": "first_name", "nullable": false },
        { "name": "last_name", "nullable": false },
        { "name": "home_addresses", "type":"addresses[]", "nullable": true }
    ],
    "data":[
        {
            "_id": 0,
            "first_name": "John",
            "last_name": "Doe",
            "home_addresses": [0, 1]
        }
    ]
}
```